package ru.fintech.homework_6;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String ARGUMENT_KEY = "ARGUMENT_KEY";

    private EditText mEditText;
    private TextView mTextView;
    private ProgressBar mProgressBar;

    private SchedulerProvider mSchedulerProvider = new AppSchedulerProvider();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText = findViewById(R.id.et_number);
        mTextView = findViewById(R.id.tv_result);
        mProgressBar = findViewById(R.id.progress_bar);
        Button calcButton = findViewById(R.id.btn_calculate);
        calcButton.setOnClickListener(this);
        Timber.plant(new Timber.DebugTree());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_calculate) {
            calculation();
        }
    }

    private void calculation() {
        String number = mEditText.getText().toString().trim();
        if (!TextUtils.isEmpty(number)) {
            clearTextField();
            showLoading(true);
            MyObservable.from(new MyCallable(number))
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .subscribe(new MyCallback() {
                        @Override
                        public void accept(final Bundle bundle) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String result = bundle.getString(ARGUMENT_KEY);
                                    mTextView.setText(result);
                                    showLoading(false);
                                }
                            });
                        }
                    });
        }
    }

    private void showLoading(boolean visibility) {
        mProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    private void clearTextField() {
        mTextView.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSchedulerProvider.clear();
    }
}
