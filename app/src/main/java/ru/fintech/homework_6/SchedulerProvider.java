package ru.fintech.homework_6;

import android.os.Looper;

public interface SchedulerProvider {

    Looper ui();

    Looper io();

    void clear();

}
