package ru.fintech.homework_6;

//    Написать класс MyObservable:
//    Выполнение стартует только после subscribe()
//    subscribeOn — на каком лупере будет выполняться callable; если не указан, выполнится на том треде, в котором вызвали subscribe
//    observeOn — на каком лупере будет выполняться callback; если не указан выполнится на треде subscribeOn
//    Порядок subscribeOn и observeOn не важен Просьба логгировать текущий тред выполнения

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import timber.log.Timber;

import static ru.fintech.homework_6.MainActivity.ARGUMENT_KEY;

public final class MyCallable {

    private static final int START_OPERATION = 1001;
    private static final int STOP_OPERATION = 2002;

    private String number;
    private MyCallback mMyCallback;
    private Looper mUiLooper;
    private Looper mIoLooper;
    private Handler mUiHandler;
    private Handler mIoHandler;

    public MyCallable(String number) {
        this.number = number;
    }

    public void setMyCallback(MyCallback mMyCallback) {
        this.mMyCallback = mMyCallback;
    }

    public void setUiLooper(Looper mUiLooper) {
        this.mUiLooper = mUiLooper;
    }

    public void setIoLooper(Looper mIoLooper) {
        this.mIoLooper = mIoLooper;
    }

    public void start() {
        if (mMyCallback != null) {
            createHandler();
            mIoHandler.obtainMessage(START_OPERATION).sendToTarget();
        }
    }

    private void startOperation() {
        try {
            Timber.d("Start operation on %s thread", Thread.currentThread().getName());
            Thread.sleep(2000);
            long num = Long.parseLong(number) * 2;
            Timber.d("Stop operation on %s thread", Thread.currentThread().getName());
            mUiHandler.obtainMessage(STOP_OPERATION, num).sendToTarget();
        } catch (InterruptedException e) {
            Timber.e(e);
        }
    }

    private void updateView(long number) {
        Timber.d("Start update view on %s thread", Thread.currentThread().getName());
        Bundle args = new Bundle();
        args.putString(ARGUMENT_KEY, String.valueOf(number));
        mMyCallback.accept(args);
        Timber.d("Stop update view on %s thread", Thread.currentThread().getName());
    }

    private void createHandler() {
        if (mIoLooper == null) {
            mIoLooper = Looper.getMainLooper();
        }
        if (mUiLooper == null) {
            mUiLooper = mIoLooper;
        }
        Callback callback = new Callback();
        mIoHandler = new Handler(mIoLooper, callback);
        mUiHandler = new Handler(mUiLooper, callback);
        Timber.d("Handlers created");
    }

    class Callback implements Handler.Callback {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case START_OPERATION :
                    startOperation();
                    break;
                case STOP_OPERATION :
                    long num = (long) message.obj;
                    updateView(num);
                    break;
            }
            return false;
        }
    }
}
