package ru.fintech.homework_6;

import android.os.HandlerThread;
import android.os.Looper;

public class AppSchedulerProvider implements SchedulerProvider {

    private HandlerThread mHandlerThread;

    @Override
    public Looper ui() {
        return Looper.getMainLooper();
    }

    @Override
    public Looper io() {
        if (mHandlerThread == null) {
            mHandlerThread = new HandlerThread("background");
        }
        if (!mHandlerThread.isAlive()) mHandlerThread.start();
        return mHandlerThread.getLooper();
    }

    @Override
    public void clear() {
        if (mHandlerThread != null) {
            mHandlerThread.quit();
        }
    }
}
