package ru.fintech.homework_6;

import android.os.Looper;
import android.support.annotation.Nullable;

//    Написать класс MyObservable:
//    Выполнение стартует только после subscribe()
//    subscribeOn — на каком лупере будет выполняться callable; если не указан, выполнится на том треде, в котором вызвали subscribe
//    observeOn — на каком лупере будет выполняться callback; если не указан выполнится на треде subscribeOn
//    Порядок subscribeOn и observeOn не важен Просьба логгировать текущий тред выполнения

//    MyObservable.from(callable)
//              .subscribeOn(looper)
//              .observeOn(mainLooper)
//              .subscribe(callback);

public abstract class MyObservable {

    public static MyObservable from(MyCallable callable) {
        return new MyObservableFromCallable(callable);
    }

    public final MyObservable subscribeOn(@Nullable Looper looper) {
        return new MyObservableSubscribeOn(looper, (MyObservableFromCallable) this);
    }

    public final MyObservable observeOn(@Nullable Looper looper) {
        return new MyObservableObserveOn(looper, (MyObservableSubscribeOn) this);
    }

    public final void subscribe(MyCallback callback) {
        subscribe(callback, this);
    }

    private void subscribe(MyCallback callback, MyObservable observable) {
        MyObservableObserveOn observeOn = (MyObservableObserveOn) observable;
        MyObservableSubscribeOn subscribeOn = observeOn.getSubscribeOn();
        MyCallable callable = subscribeOn.getMyObservableFromCallable().getCallable();
        if (subscribeOn.getIoLooper() != null) {
            callable.setIoLooper(subscribeOn.getIoLooper());
        }
        if (observeOn.getUiLooper() != null) {
            callable.setUiLooper(observeOn.getUiLooper());
        }
        callable.setMyCallback(callback);
        callable.start();
    }

    private static class MyObservableFromCallable extends MyObservable {

        private final MyCallable mCallable;

        public MyObservableFromCallable(MyCallable callable) {
            this.mCallable = callable;
        }

        public MyCallable getCallable() {
            return mCallable;
        }
    }

    private static class MyObservableObserveOn extends MyObservable {

        private final Looper mUiLooper;
        private final MyObservableSubscribeOn mSubscribeOn;

        public MyObservableObserveOn(Looper looper, MyObservableSubscribeOn source) {
            this.mUiLooper = looper;
            this.mSubscribeOn = source;
        }

        public Looper getUiLooper() {
            return mUiLooper;
        }

        public MyObservableSubscribeOn getSubscribeOn() {
            return mSubscribeOn;
        }
    }

    private static class MyObservableSubscribeOn extends MyObservable {

        private final Looper mIoLooper;
        private final MyObservableFromCallable mMyObservableFromCallable;

        public MyObservableSubscribeOn(Looper looper, MyObservableFromCallable source) {
            this.mIoLooper = looper;
            this.mMyObservableFromCallable = source;
        }

        public Looper getIoLooper() {
            return mIoLooper;
        }

        public MyObservableFromCallable getMyObservableFromCallable() {
            return mMyObservableFromCallable;
        }
    }
}
