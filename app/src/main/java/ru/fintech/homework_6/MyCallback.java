package ru.fintech.homework_6;

import android.os.Bundle;

public interface MyCallback {

    void accept(Bundle bundle);

}
