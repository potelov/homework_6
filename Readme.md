Написать класс MyObservable:

MyObservable.from(callable)
    .subscribeOn(looper)
    .observeOn(mainLooper)
    .subscribe(callback);

1. Выполнение стартует только после subscribe()
2. subscribeOn — на каком лупере будет выполняться callable; если не указан, выполнится на том треде, в котором вызвали subscribe
3. observeOn — на каком лупере будет выполняться callback; если не указан выполнится на треде subscribeOn
4. Порядок subscribeOn и observeOn не важен
5. callable и callback — собственные классы
6. Просьба логгировать текущий тред выполнения